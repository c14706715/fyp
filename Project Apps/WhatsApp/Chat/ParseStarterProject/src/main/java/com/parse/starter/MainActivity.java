/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;


public class MainActivity extends Activity {

  Boolean loginModeActive = false;

  public void RedirectIfLoggedIn(){
      if(ParseUser.getCurrentUser() != null){
          Intent intent = new Intent(getApplicationContext(), UserListActivity.class);
          startActivity(intent);
      }
  }

  public void toggleLoginMode(View view){

      Button loginSignUpBtn = (Button) findViewById(R.id.SignUpBtn);
      TextView toggleLoginModeTextView = (TextView) findViewById(R.id.toggleLoginModeTextView);


      if(loginModeActive){
          loginModeActive = false;
          loginSignUpBtn.setText("Sign Up");
          toggleLoginModeTextView.setText(("Or, Log In"));

      }
      else{
          loginModeActive = true;
          loginSignUpBtn.setText("Log In");
          toggleLoginModeTextView.setText("Or, Sign Up");
      }
  }

  public void SignUpLog(View view) {

        EditText usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        EditText passwordEditText = (EditText) findViewById(R.id.PasswordEditText);

        if(loginModeActive) {
            ParseUser.logInInBackground(usernameEditText.getText().toString(), passwordEditText.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if(e == null){
                        Log.i("Info", "User logged in");
                        RedirectIfLoggedIn();
                    }
                    else{
                        String message = e.getMessage();

                        if(message.toLowerCase().contains("java")){
                            message =e.getMessage().substring(e.getMessage().indexOf(" "));
                        }
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else{
            ParseUser user = new ParseUser();
            user.setUsername(usernameEditText.getText().toString());
            user.setPassword(passwordEditText.getText().toString());

            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                      if (e == null) {
                          Log.i("Info", "User signed up");
                          RedirectIfLoggedIn();
                      }
                      else {
                          String message = e.getMessage();

                          if(message.toLowerCase().contains("java")){
                              message =e.getMessage().substring(e.getMessage().indexOf(" "));
                          }
                          Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                      }
                }
            });
        }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    setTitle("WhatsApp Login");

      RedirectIfLoggedIn();

    ParseUser currentUser = ParseUser.getCurrentUser();
    currentUser.logOut();

    
    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }

}