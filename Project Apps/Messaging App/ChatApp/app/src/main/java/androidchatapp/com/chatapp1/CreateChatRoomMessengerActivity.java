package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidchatapp.com.chatapp1.R;

/*
    Jake Young
    C14706715
    DT282
*/


public class CreateChatRoomMessengerActivity extends AppCompatActivity {

    //Declare the objects needed
    Button  Add_Room_Btn, ReturnMainMenu;
    private EditText ETRoomName;
    private EditText ETAddressLine1;
    private EditText ETAddressLine2;
    private EditText ETAddressTown;
    private EditText ETAddressCounty;
    private ArrayList<String> Room_List = new ArrayList<>();
    String Security_Code;
    String Location_Address;

    //Connection to the firebase Database
    DatabaseReference RootRef = FirebaseDatabase.getInstance().getReference();

    //Used to encode the address into Latitude and Longitude so that google Map Directions can
    //read the co-ordinates
    public void Geocode_LatLng(String Room_Name, String Full_Address) throws IOException {

        String Geocode_Address = Full_Address;
        Geocoder geocoder = new Geocoder(this);

        //Initialising the references for the the firebase database
        FirebaseDatabase RootReference = FirebaseDatabase.getInstance();

        //Gets reference to the Users database
        final DatabaseReference UserReference = RootReference.getReference("Group_Chats");

        //API function to get lat and lng
        List<Address> list = geocoder.getFromLocationName(Geocode_Address, 1);

        //put the first value in the array into Address
        Address address = list.get(0);

        //Retrieve the values from Address
        double lat = address.getLatitude();
        double lng = address.getLongitude();

        //Push the values to the users database
        UserReference.child(ETRoomName.getText().toString()).child("AddressLatitude").setValue(lat);
        UserReference.child(ETRoomName.getText().toString()).child("AddressLongitude").setValue(lng);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_chat_room_messenger);


        //Retrieving the Email from Login activity for setting username
        Intent RetrieveIntent = getIntent();
        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");

        //Initialising the references for the the firebase database
        FirebaseDatabase RootReference = FirebaseDatabase.getInstance();

        //Gets reference to the Users database
        final DatabaseReference RoomReference = RootReference.getReference("Group_Chats");

        //Connecting objects to values in the XML file
        Add_Room_Btn = findViewById(R.id.btn_add_room);
        ETRoomName =  findViewById(R.id.ETRoomName);
        ETAddressLine1 = findViewById(R.id.ETAddressLine1);
        ETAddressLine2 = findViewById(R.id.ETAddressLine2);
        ETAddressTown = findViewById(R.id.ETAddressTown);
        ETAddressCounty = findViewById(R.id.ETAddressCounty);
        ReturnMainMenu = findViewById(R.id.ReturnMainMenuCreate);

        ReturnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //.putExtra allows to send the LoginEmail
                Intent intent = new Intent(CreateChatRoomMessengerActivity.this, MenuActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        //Error Checking
        //Log.i("Create Chat Email", " "+LoginEmail);

        Add_Room_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check if room-name is empty if so do not allow user to proceed
                final String Room_Name_Checker = ETRoomName.getText().toString().trim();
                final String Address_Line_1_Checker = ETAddressLine1.getText().toString().trim();
                final String Address_Line_2_Checker = ETAddressLine2.getText().toString().trim();
                final String Address_Town_Checker = ETAddressTown.getText().toString().trim();
                final String Address_County_Checker = ETAddressCounty.getText().toString().trim();

                //Checking for empty room name
                if (TextUtils.isEmpty(Room_Name_Checker)) {
                    Toast.makeText(getApplicationContext(),
                            "You have not filled in room name\nPlease try again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateChatRoomMessengerActivity.this, CreateChatRoomMessengerActivity.class);
                    startActivity(intent);
                }
                if (TextUtils.isEmpty(Address_Line_1_Checker)) {
                    Toast.makeText(getApplicationContext(),
                            "You have not filled in address line 1\nPlease try again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateChatRoomMessengerActivity.this, CreateChatRoomMessengerActivity.class);
                    startActivity(intent);
                }
                if (TextUtils.isEmpty(Address_Line_2_Checker)) {
                    Toast.makeText(getApplicationContext(),
                            "You have not filled in address line 2\nPlease try again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateChatRoomMessengerActivity.this, CreateChatRoomMessengerActivity.class);
                    startActivity(intent);
                }
                if (TextUtils.isEmpty(Address_Town_Checker)) {
                    Toast.makeText(getApplicationContext(),
                            "You have not filled in town\nPlease try again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateChatRoomMessengerActivity.this, CreateChatRoomMessengerActivity.class);
                    startActivity(intent);
                }
                else if (TextUtils.isEmpty(Address_County_Checker)) {
                    Toast.makeText(getApplicationContext(),
                            "You have not filled in county\nPlease try again", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CreateChatRoomMessengerActivity.this, CreateChatRoomMessengerActivity.class);
                    startActivity(intent);
                }
                else {
                    Location_Address =
                            Address_Line_1_Checker + ", " +
                                    Address_Line_2_Checker + ", " +
                                    Address_Town_Checker + ", " +
                                    Address_County_Checker;

                    //Creating a random Alpha-numerical string with a length of 8 digits
                    Security_Code = GenerateRandomString.randomString(8);

                    //Reference to database
                    DatabaseReference RoomNameRef = RootRef.child("Group_Chats").child(Room_Name_Checker);

                    //Checking if room name chosen already exists
                    ValueEventListener eventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot1) {
                            //If so post a Toast
                            if (dataSnapshot1.exists()) {
                                Toast.makeText(getApplicationContext(),
                                        "Sorry, \n" +
                                                "This room name has been taken\n" +
                                                "Please try another name", Toast.LENGTH_LONG).show();
                                ETRoomName.setText("");
                                return;
                            }
                            //Else follow through with adding group to DB
                            else {
                                //Putting the group and attaching the security code to it in the database
                                RoomReference.child(ETRoomName.getText().toString());
                                RoomReference.child(ETRoomName.getText().toString()).child("Security_Code").setValue(Security_Code);
                                RoomReference.child(ETRoomName.getText().toString()).child("Location_Address").setValue(Location_Address);

                                //In a try catch due to IO Exception
                                try {
                                    //Call the geocode method with the variables returned to the function
                                    Geocode_LatLng(Room_Name_Checker, Location_Address);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Toast.makeText(getApplicationContext(),
                                        "Congratulations you have created a new room\nYour unique security code is " +
                                                Security_Code, Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(CreateChatRoomMessengerActivity.this, PeopleListActivity.class)
                                        .putExtra("Security_Code", Security_Code)
                                        .putExtra("LoginEmail", LoginEmail);

                                //Pushing necessary values to the PeopleListActivity
                                intent.putExtra("Room_Name", ETRoomName.getText().toString());
                                intent.putExtra("Login_Email", LoginEmail);
                                intent.putExtra("Security_Code", Security_Code);
                                //Resetting EditText to empty
                                ETRoomName.setText("");
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    };
                    RoomNameRef.addListenerForSingleValueEvent(eventListener);
                }
            }
        });
    }
}
