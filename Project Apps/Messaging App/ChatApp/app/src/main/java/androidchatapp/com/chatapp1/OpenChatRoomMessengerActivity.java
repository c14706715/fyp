package androidchatapp.com.chatapp1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;


/*
    Jake Young
    C14706715
    DT282/4
 */

public class OpenChatRoomMessengerActivity extends AppCompatActivity {

    //Declare objects needed
    ListView View_Rooms_View;
    Button ReturnToMainMenu;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> Room_List = new ArrayList<>();
    final Context context = this;
    String SecurityCodeResult;
    TextView Room_Name_From_List;
    String RN;
    String FirebaseSecurityCodeString;

    //Connection to the firebase database
    private DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_chat_room_messenger);

        //Retrieving the login email from user in login activity  and setting as LoginEmail
        Intent RetrieveIntent = getIntent();

        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");

        //Check if variable is being correctly passed between activities
        //Log.i("OC-OpenChat Email", " "+LoginEmail);

        View_Rooms_View = findViewById(R.id.listView);

        ReturnToMainMenu = findViewById(R.id.ReturnMainMenuOpen);

        ReturnToMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //.putExtra allows to send the LoginEmail
                Intent intent = new Intent(OpenChatRoomMessengerActivity.this, MenuActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        //Setting the standard list layout
        arrayAdapter = new ArrayAdapter<>(this, R.layout.list_layout ,Room_List);


        //Attaching to the view
        View_Rooms_View.setAdapter(arrayAdapter);

        //Retrieve data from the database
        root.child("Group_Chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Set<String> set = new HashSet<>();
                Iterator i = dataSnapshot.getChildren().iterator();

                while (i.hasNext()){
                    set.add(((DataSnapshot)i.next()).getKey());
                }

                Room_List.clear();
                Room_List.addAll(set);

                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        View_Rooms_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                //Creating prompt view to ask user for security code
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.security_prompt, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setView(promptsView);

                //Setting variable to connected xml id
                final EditText EnteredSecurityCode = promptsView.findViewById(R.id.EnteredSecurityCodeEditText);
                Room_Name_From_List = (TextView)view;
                RN = Room_Name_From_List.getText().toString();

                //Log used just for error checking
                //Log.i("OC-RN", " " + RN);

                //Created String array to access Firebase value outside of the root method
                final String[] FirebaseSecurityCode = new String[1];

                //Retrieve data from the database
                root.child("Group_Chats").addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //Getting the Firebase Security Code
                        FirebaseSecurityCode[0] = dataSnapshot.child(RN).child("Security_Code").getValue().toString();

                        //Converting the array value to a String
                        FirebaseSecurityCodeString = FirebaseSecurityCode[0];

                        //Display in Log - error checking
                        Log.i("OC-FirebaseSecurityCode", ""+ FirebaseSecurityCode[0]);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        //Parsing the edit text content to string
                                        SecurityCodeResult = EnteredSecurityCode.getText().toString();

                                        //Error checking
                                        //Log.i("OC-Entered SecurityCode", ""+ SecurityCodeResult);
                                        //Log.i("OC-Firebase SecuriCode2", ""+ FirebaseSecurityCodeString);

                                        if(Objects.equals(SecurityCodeResult, FirebaseSecurityCodeString)){
                                            Intent intent = new Intent(getApplicationContext(), Chat_Room.class).putExtra("LoginEmail", LoginEmail);
                                            intent.putExtra("Room_Name", ((TextView)view).getText().toString() );
                                            intent.putExtra("LoginEmail", LoginEmail);
                                            startActivity(intent);
                                        }
                                        else{
                                            Toast.makeText(getApplicationContext(), "Error, Security code is incorrect\nPlease try again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                        //on click cancel...
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

    }



}