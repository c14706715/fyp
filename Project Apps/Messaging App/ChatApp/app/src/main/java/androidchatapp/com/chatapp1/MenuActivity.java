package androidchatapp.com.chatapp1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by Jake on 13/12/2017.
 */

public class MenuActivity extends Activity {

    //Declaring buttons
    Button CreateAGroupChat, OpenAGroupChat, LoginBtn, AccountBtn;
    TextView LoginEmailTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);

        //Initialising buttons
        CreateAGroupChat = findViewById(R.id.CreateAGroupChat);
        OpenAGroupChat = findViewById(R.id.OpenAGroupChat);
        LoginBtn = findViewById(R.id.LoginBtn);
        AccountBtn = findViewById(R.id.AccountBtn);
        LoginEmailTV = findViewById(R.id.LoginEmailTV);

        //Retrieving the loginEmail from the login activity and other activities
        Intent RetrieveIntent = getIntent();
        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");

        //Simple onClick methods to navigate to certain activities
        OpenAGroupChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //.putExtra allows to send the LoginEmail
                Intent intent = new Intent(MenuActivity.this, OpenChatRoomMessengerActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        CreateAGroupChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, CreateChatRoomMessengerActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, LoginActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        AccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, AccountActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });
    }
}
