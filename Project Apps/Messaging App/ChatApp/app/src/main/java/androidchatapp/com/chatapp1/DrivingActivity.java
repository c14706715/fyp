package androidchatapp.com.chatapp1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
//import android.util.Log;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Map_Classes.DirectionFinder;
import Map_Classes.DirectionFinderListener;
import Map_Classes.Route;



import static android.Manifest.permission.ACCESS_FINE_LOCATION;


/**
 * Created by Jake on 22/12/2017.
 */

public class DrivingActivity extends FragmentActivity implements OnMapReadyCallback, DirectionFinderListener {


    private static final int PERMISSION_REQUEST_CODE = 200;


    String FullUserEmailList, LoginEmail1;
    String Start_Address, user1, user2, user3, user4, Location_Address;
    String Eircode1, Eircode2, Eircode3, Eircode4;
    String User1Address, User2Address, User3Address, User4Address, DriverAddress, DestinationAddress;
    String RouteDirections;
    LatLng DestinationLatLng;
    String Destination;

    private GoogleMap mMap;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Marker> waypointMarkers1 = new ArrayList<>();
    private List<Marker> waypointMarkers2 = new ArrayList<>();
    private List<Marker> waypointMarkers3 = new ArrayList<>();
    private List<Marker> waypointMarkers4 = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;

    String OutRoomName="";

    //Connection to the firebase database
    DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    //setting the original destination to Arklow
    LatLng Arklow = new LatLng(52.797693, -6.159929);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driving_layout);

        //Retrieves the map from the XML so that I can add Markers etc.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //Retrieving the Room name from user in Chat Room and setting as Room_Name
        Intent RetrieveIntent = getIntent();
        final String Room_Name = RetrieveIntent.getStringExtra("Room_Name");
        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");


        OutRoomName = Room_Name;
        //Splitting the email string so it can be used to access the DB at a later stage
        String[] Email_Part_1 = LoginEmail.split("@");
        LoginEmail1 = Email_Part_1[0];


        final String[] EmailList = new String[1];
        final String[] Final_Location_Address = new String[1];
        final String[] Final_Start_Address = new String[1];

        final String[] Final_Destination_Address = new String[1];

        Button btnFindPath =  findViewById(R.id.btnFindPath);
        Button DirectionsBtn = findViewById(R.id.DirectionsBtn);

        DirectionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if no users are added to the group
                if(EmailList[0] == null){
                    Toast.makeText(getApplicationContext(), "Please add at least one person", Toast.LENGTH_LONG).show();
                }
                //request method to send the LatLng co-ords to the map
                sendRequest();

                //Open the Directions Activity
                Intent intent = new Intent(DrivingActivity.this, DirectionsActivity.class)
                        .putExtra("Room_Name", Room_Name)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
            }
        });

        btnFindPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //request method to send the LatLng co-ords to the map
                sendRequest();
            }
        });


        //Retrieve data from the database
        root.child("Group_Chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Final_Location_Address[0] = dataSnapshot.child(Room_Name).child("Location_Address").getValue().toString();

                //Converting the array value to a String
                Location_Address = Final_Location_Address[0];

                //Error Check
                //Log.i("D - In End Location 1", " " + Final_Location_Address[0]);

                //Error Check
                //Log.i("D - In End Location 2", " " + Location_Address);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Retrieve data from the database
        root.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.i("Log", ""+LoginEmail1);
                //Log.i("Log", ""+dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString());
                Final_Start_Address[0] = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();

                //Converting the array value to a String
                Start_Address = Final_Start_Address[0];


                //Error Check
                //Log.i("D - In Start Location 1", " " + Final_Start_Address[0]);

                //Error Check
                //Log.i("D - In Start Location 2", " " + Start_Address);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Error Check
        //Log.i("D - Start Location", " " + Start_Address);

        //Error Check
        //Log.i("D - Location", " " + Location_Address);

        //Error Check
        //Log.i("D - Login Email 1", " " + LoginEmail1);

        //Error Check
        //Log.i("D - Room Name", Room_Name);

        //Retrieve data from the database
        root.child("Group_Chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Final_Destination_Address[0] = dataSnapshot.child(Room_Name).child("Location_Address").getValue().toString();

                DestinationAddress = Final_Destination_Address[0];

                //Log.i("DestinationADDR", ""+DestinationAddress);
                //Error Check
                //Log.i("D - Room Name", " " + Room_Name);

                EmailList[0] = dataSnapshot.child(Room_Name).child("List_Of_Users").getValue().toString();

                //Error Checking
                //Log.i("D - Email List", "" + EmailList[0]);

                //Convert to string to reach outside method
                FullUserEmailList = EmailList[0];

                //Removing the Driver from the least as it is redunedant info
                FullUserEmailList = FullUserEmailList.replace(LoginEmail1, "");

                //Error Check
                //Log.i("D - User Email List", "" + FullUserEmailList);

                FullUserEmailList = FullUserEmailList.replace("[", "");

                //Error checking
                //Log.i("D - Full Email List 2", "" + FullUserEmailList);

                FullUserEmailList = FullUserEmailList.replace("]", "");

                FullUserEmailList = FullUserEmailList.replace(" ,", "");

                //Error checking
                //Log.i("D - Full Email List 3", "" + FullUserEmailList);

                //Convert FullUserEmailList to array list seperating each email
                List<String> list = new ArrayList<>(Arrays.asList(FullUserEmailList.split(", ")));

                //way of counting num of extra people to collect
                int Counter = 1;
                for(char c : FullUserEmailList.toCharArray()){
                    if(c == ','){
                        Counter++;
                    }
                }

                //Log.i("Counter", ""+Counter);
                for(int i=0; i<list.size(); i++){
                    //Log.i("List: "+i, " "+list.get(i));
                }

                if(Counter == 1){
                    //Log.i("users:", ""+list.get(0));
                    user1 = list.get(0);
                }
                if(Counter == 2){
                    //Log.i("users:", ""+list.get(0));
                    //Log.i("users:", ""+list.get(1));
                    user1 = list.get(0);
                    user2 = list.get(1);

                    if(user1.length() < 2 ){
                        user1 = user2;
                        Counter --;
                    }
                }
                else if(Counter == 3){
                    //Log.i("users:", ""+list.get(0));
                    //Log.i("users:", ""+list.get(1));
                    //Log.i("users:", ""+list.get(2));
                    user1 = list.get(0);
                    user2 = list.get(1);
                    user3 = list.get(2);
                }
                else if (Counter == 4){
                    //Log.i("users:", ""+list.get(0));
                    //Log.i("users:", ""+list.get(1));
                    //Log.i("users:", ""+list.get(2));
                    //Log.i("users:", ""+list.get(3));
                    user1 = list.get(0);
                    user2 = list.get(1);
                    user3 = list.get(2);
                    user4 = list.get(3);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        root.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(user1 == null){
                    //Retrieve Eircode Address
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine1").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine2").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressTown").getValue().toString();
                }
                if(user2 == null){
                    //Retrieve Eircode Address
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine1").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine2").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressTown").getValue().toString();

                    Eircode1 = dataSnapshot.child(user1).child("Address").child("AddressEircode").getValue().toString();
                    User1Address = dataSnapshot.child(user1).child("Address").child("AddressLine1").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressLine2").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressTown").getValue().toString();

                    //if there is no user set to null
                    Eircode2 = "1";
                    Eircode3 = "1";
                    Eircode4 = "1";
                }
                else if(user3 == null){
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine1").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine2").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressTown").getValue().toString();

                    Eircode1 = dataSnapshot.child(user1).child("Address").child("AddressEircode").getValue().toString();
                    User1Address = dataSnapshot.child(user1).child("Address").child("AddressLine1").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressLine2").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressTown").getValue().toString();

                    if(user2 != LoginEmail1) {

                        Eircode2 = dataSnapshot.child(user2).child("Address").child("AddressEircode").getValue().toString();
                        User2Address = dataSnapshot.child(user2).child("Address").child("AddressLine1").getValue().toString();
                        User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressLine2").getValue().toString();
                        User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressTown").getValue().toString();
                    }
                    //if there is no user set to null

                    Eircode3 = "1";
                    Eircode4 = "1";
                }
                else if(user4 == null){
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine1").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine2").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressTown").getValue().toString();

                    Eircode1 = dataSnapshot.child(user1).child("Address").child("AddressEircode").getValue().toString();
                    User1Address = dataSnapshot.child(user1).child("Address").child("AddressLine1").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressLine2").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressTown").getValue().toString();

                    Eircode2 = dataSnapshot.child(user2).child("Address").child("AddressEircode").getValue().toString();
                    User2Address = dataSnapshot.child(user2).child("Address").child("AddressLine1").getValue().toString();
                    User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressLine2").getValue().toString();
                    User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressTown").getValue().toString();

                    Eircode3 = dataSnapshot.child(user3).child("Address").child("AddressEircode").getValue().toString();
                    User3Address = dataSnapshot.child(user3).child("Address").child("AddressLine1").getValue().toString();
                    User3Address = User3Address + ", " + dataSnapshot.child(user3).child("Address").child("AddressLine2").getValue().toString();
                    User3Address = User3Address + ", " + dataSnapshot.child(user3).child("Address").child("AddressTown").getValue().toString();

                    //If there is no user set to null
                    Eircode4 = "1";
                }
                else {
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressEircode").getValue().toString();
                    DriverAddress = dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine1").getValue().toString();
                    DriverAddress = DriverAddress + ", >" + dataSnapshot.child(LoginEmail1).child("Address").child("AddressLine2").getValue().toString();
                    DriverAddress = DriverAddress + ", " + dataSnapshot.child(LoginEmail1).child("Address").child("AddressTown").getValue().toString();

                    Eircode1 = dataSnapshot.child(user1).child("Address").child("AddressEircode").getValue().toString();
                    User1Address = dataSnapshot.child(user1).child("Address").child("AddressLine1").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressLine2").getValue().toString();
                    User1Address = User1Address + ", " + dataSnapshot.child(user1).child("Address").child("AddressTown").getValue().toString();

                    Eircode2 = dataSnapshot.child(user2).child("Address").child("AddressEircode").getValue().toString();
                    User2Address = dataSnapshot.child(user2).child("Address").child("AddressLine1").getValue().toString();
                    User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressLine2").getValue().toString();
                    User2Address = User2Address + ", " + dataSnapshot.child(user2).child("Address").child("AddressTown").getValue().toString();

                    Eircode3 = dataSnapshot.child(user3).child("Address").child("AddressEircode").getValue().toString();
                    User3Address = dataSnapshot.child(user3).child("Address").child("AddressLine1").getValue().toString();
                    User3Address = User3Address + ", " + dataSnapshot.child(user3).child("Address").child("AddressLine2").getValue().toString();
                    User3Address = User3Address + ", " + dataSnapshot.child(user3).child("Address").child("AddressTown").getValue().toString();

                    Eircode4 = dataSnapshot.child(user4).child("Address").child("AddressEircode").getValue().toString();
                    User4Address = dataSnapshot.child(user4).child("Address").child("AddressLine1").getValue().toString();
                    User4Address = User4Address + ", " + dataSnapshot.child(user4).child("Address").child("AddressLine2").getValue().toString();
                    User4Address = User4Address + ", " + dataSnapshot.child(user4).child("Address").child("AddressTown").getValue().toString();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //Log.i("eircode 1 " + Eircode1, "eircode 2 " + Eircode2);
        //Log.i("User 1 " + user1, "User 2 " + user2);
    }

    //Method to send request of coords to the method
    private void sendRequest() {
        try {
            //Converting the address of the destination to LatLng then to string in format for GoogleMapsAPI preferable style
            DestinationLatLng = getLocationFromAddress(this, DestinationAddress);
            //Log.i("DestLATLNG", ""+DestinationLatLng);
            Destination = DestinationLatLng.latitude + "," +DestinationLatLng.longitude;
            new DirectionFinder(this, Start_Address, Destination, Eircode1, Eircode2, Eircode3, Eircode4).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Arklow, 8));

        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        this, android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
    }


    @Override
    public void onDirectionFinderStart() {
        //set to display when setting the map
        progressDialog = ProgressDialog.show(this,
                "KiddieCarpool is working out a route",
                "...Please wait...",
                true);
    }

    //Encoding Address to LatLng
    public LatLng getLocationFromAddress(Context context, String strAddress)
    {
        Geocoder coder= new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 5);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        //returning the encoded Address in LatLng format
        return p1;
    }


    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        waypointMarkers1 = new ArrayList<>();
        waypointMarkers2 = new ArrayList<>();
        waypointMarkers4 = new ArrayList<>();

        for (Route route : routes) {

            //root.child("Group_Chats").child(OutRoomName).child("DirectionInstruction").setValue(route.directions.text);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            ((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);


            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon))
                    .title(DriverAddress)
                    .position(getLocationFromAddress(this, Start_Address))));


            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.destination_icon))
                    .title(DestinationAddress)
                    .position(getLocationFromAddress(this, Location_Address))));

            waypointMarkers1.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_icon))
                    .title(User1Address)
                    .position(getLocationFromAddress(this, Eircode1))));

            waypointMarkers2.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_icon))
                    .title(User2Address)
                    .position(getLocationFromAddress(this, Eircode2))));

            waypointMarkers3.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pink_icon))
                    .title(User3Address)
                    .position(getLocationFromAddress(this, Eircode3))));

            waypointMarkers4.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.silver_icon))
                    .title(User4Address)
                    .position(getLocationFromAddress(this, Eircode4))));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}