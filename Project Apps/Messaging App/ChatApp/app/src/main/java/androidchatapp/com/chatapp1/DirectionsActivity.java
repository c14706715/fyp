package androidchatapp.com.chatapp1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mapbox.geojson.Point;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
//import android.util.Log;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.services.android.navigation.ui.v5.NavigationView;
import com.mapbox.services.android.navigation.ui.v5.NavigationViewOptions;
import com.mapbox.services.android.navigation.ui.v5.OnNavigationReadyCallback;
import com.mapbox.services.android.navigation.ui.v5.listeners.NavigationListener;
import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener;
import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress;
import com.mapbox.services.android.navigation.v5.utils.RouteUtils;

import Map_Classes.Directions;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class DirectionsActivity extends AppCompatActivity implements OnNavigationReadyCallback,
        NavigationListener, ProgressChangeListener {


    private static final int PERMISSION_REQUEST_CODE = 200;
    private View view;

    //Create the needed variables
    private NavigationView navigationView;
    private boolean dropoffDialogShown;
    private Location lastKnownLocation;
    private List<Point> points = new ArrayList<>();
    String user1, user2, user3, user4, LoginEmail;
    Double Address1Lat, Address1Lng, Address2Lat, Address2Lng,
            Address3Lat, Address3Lng, Address4Lat, Address4Lng,
            DriverAddressLat, DriverAddressLng, DestinationLat, DestinationLng;


    //Connection to the firebase database
    DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppCompat_NoActionBar);
        super.onCreate(savedInstanceState);

        //Create variable
        final String[] EmailList = new String[1];
        final String[] DesLat = new String[1];
        final String[] DesLng = new String[1];

        //Retrieve the key to enable navigation for mapbox
        Mapbox.getInstance(this, getString(R.string.access_token));

        //Retrieving the Room name from user in Chat Room and setting as Room_Name
        Intent RetrieveIntent = getIntent();
        final String Room_Name = RetrieveIntent.getStringExtra("Room_Name");
        final String LoginEmail1 = RetrieveIntent.getStringExtra("LoginEmail");

        //Splitting the email string so it can be used to access the DB at a later stage
        String[] Email_Part_1 = LoginEmail1.split("@");

        //convert to outer string
        LoginEmail = Email_Part_1[0];

        //Error Checking
        //Log.i("Directions", ""+Room_Name);

        AlertDialog alertDialog = new AlertDialog.Builder(DirectionsActivity.this).create();
        alertDialog.setMessage("Allow KiddieCarpool to access your location");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Enable Location",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int in) {
                        ActivityCompat.requestPermissions(DirectionsActivity.this, new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                    }
                });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Don't Allow",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int in) {
                        Intent intent = new Intent(DirectionsActivity.this, MenuActivity.class)
                                .putExtra("LoginEmail", LoginEmail);
                        startActivity(intent);
                    }
                });

        alertDialog.show();

        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);

        if (result == PackageManager.PERMISSION_GRANTED) {
            // Permission has already been granted
           //Log.i("RoomName", " "+Room_Name);

            //Retrieve data from the database
            root.child("Group_Chats").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    DesLat[0] = dataSnapshot.child(Room_Name).child("AddressLatitude").getValue().toString();
                    DesLng[0] = dataSnapshot.child(Room_Name).child("AddressLongitude").getValue().toString();

                    //Log.i("DesLat" + DesLat[0], "DesLng" + DesLng[0]);
                    DestinationLat = Double.valueOf(DesLat[0]);
                    DestinationLng = Double.valueOf(DesLng[0]);
                    //Log.i("DestLat" + DestinationLat, "DetLng" + DestinationLng);

                    //Log.i("Lat"+DestinationLat, "Lng"+DestinationLng);
                    EmailList[0] = dataSnapshot.child(Room_Name).child("List_Of_Users").getValue().toString();
                    //Error Checking
                    //Log.i("Dir - Email List", "" + EmailList[0]);


                    //removing unnessecary characters
                    EmailList[0] = EmailList[0].replace("[", "");

                    EmailList[0] = EmailList[0].replace("]", "");

                    EmailList[0] = EmailList[0].replace(" ,", "");

                    //Error checking
                    //Log.i("Direction-FulEmailList", "" + EmailList[0]);

                    //Convert FullUserEmailList to array list seperating each email
                    List<String> list = new ArrayList<>(Arrays.asList(EmailList[0].split(", ")));


                    //way of counting num of extra people to collect
                    int Counter = 0;
                    for (char c : EmailList[0].toCharArray()) {
                        if (c == ',') {
                            Counter++;
                        }
                    }

                    //Error checking
                    for (int i = 0; i < list.size(); i++) {
                        //Log.i("List: "+i, " "+list.get(i));
                    }

                    //Assign according to num of users
                    if (Counter == 1) {
                        user1 = list.get(0);
                    } else if (Counter == 2) {
                        user1 = list.get(0);
                        user2 = list.get(1);
                    } else if (Counter == 3) {
                        user1 = list.get(0);
                        user2 = list.get(1);
                        user3 = list.get(2);
                    } else if (Counter == 4) {
                        user1 = list.get(0);
                        user2 = list.get(1);
                        user3 = list.get(2);
                        user4 = list.get(3);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });


            //Log.i("Room", "" + Room_Name);
            //Log.i("Des=" + DestinationLat, "" + DestinationLng);

            root.child("Users").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (user1 == null) {
                        //Add to waypoint list
                        points.add(Point.fromLngLat(DestinationLng, DestinationLat));
                    }
                    if (user2 == null) {
                        //Retrieve Eircode Address
                        DriverAddressLat = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Latitude").getValue().toString());
                        DriverAddressLng = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Longitude").getValue().toString());
                        Address1Lat = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Latitude").getValue().toString());
                        Address1Lng = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Longitude").getValue().toString());

                        if(Address1Lat == null && Address1Lng == null){
                            Address1Lat = Double.valueOf(dataSnapshot.child(EmailList[0]).child("Address").child("Latitude").getValue().toString());
                            Address1Lng = Double.valueOf(dataSnapshot.child(EmailList[0]).child("Address").child("Longitude").getValue().toString());
                        }

                        //Add to waypoint list
                        points.add(Point.fromLngLat(Address1Lng, Address1Lat));
                        points.add(Point.fromLngLat(DestinationLng, DestinationLat));

                    } else if (user3 == null) {
                        //Retrieve Eircode Address
                        DriverAddressLat = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Latitude").getValue().toString());
                        DriverAddressLng = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Longitude").getValue().toString());
                        Address1Lat = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Latitude").getValue().toString());
                        Address1Lng = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Longitude").getValue().toString());
                        Address2Lat = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Latitude").getValue().toString());
                        Address2Lng = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Longitude").getValue().toString());

                        Location Loc0 = new Location("");
                        Location Loc1 = new Location("");
                        Location Loc2 = new Location("");
                        Loc0.setLatitude(DriverAddressLat);
                        Loc0.setLongitude(DriverAddressLng);
                        Loc1.setLatitude(Address1Lat);
                        Loc1.setLongitude(Address1Lng);
                        Loc2.setLatitude(Address2Lat);
                        Loc2.setLongitude(Address2Lng);


                        Double tempLat, tempLng;
                        float distance1 = Loc0.distanceTo(Loc1);
                        float distance2 = Loc0.distanceTo(Loc2);

                        if (distance1 > distance2) {
                            tempLat = Address1Lat;
                            tempLng = Address1Lng;
                            Address1Lat = Address2Lat;
                            Address1Lng = Address2Lng;
                            Address2Lat = tempLat;
                            Address2Lng = tempLng;
                        }

                        //Add to waypoint list
                        points.add(Point.fromLngLat(Address1Lng, Address1Lat));
                        points.add(Point.fromLngLat(Address2Lng, Address2Lat));
                        points.add(Point.fromLngLat(DestinationLng, DestinationLat));

                    } else if (user4 == null) {
                        //Retrieve Eircode Address
                        DriverAddressLat = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Latitude").getValue().toString());
                        DriverAddressLng = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Longitude").getValue().toString());
                        Address1Lat = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Latitude").getValue().toString());
                        Address1Lng = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Longitude").getValue().toString());
                        Address2Lat = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Latitude").getValue().toString());
                        Address2Lng = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Longitude").getValue().toString());
                        Address3Lat = Double.valueOf(dataSnapshot.child(user3).child("Address").child("Latitude").getValue().toString());
                        Address3Lng = Double.valueOf(dataSnapshot.child(user3).child("Address").child("Longitude").getValue().toString());

                        Location Loc0 = new Location("");
                        Location Loc1 = new Location("");
                        Location Loc2 = new Location("");
                        Location Loc3 = new Location("");
                        Loc0.setLatitude(DriverAddressLat);
                        Loc0.setLongitude(DriverAddressLng);
                        Loc1.setLatitude(Address1Lat);
                        Loc1.setLongitude(Address1Lng);
                        Loc2.setLatitude(Address2Lat);
                        Loc2.setLongitude(Address2Lng);
                        Loc3.setLatitude(Address3Lat);
                        Loc3.setLongitude(Address3Lng);


                        Double tempLat, tempLng;
                        float distance1 = Loc0.distanceTo(Loc1);
                        float distance2 = Loc0.distanceTo(Loc2);
                        float distance3 = Loc0.distanceTo(Loc3);

                        if (distance1 > distance2 && distance1 > distance3) {
                            tempLat = Address1Lat;
                            tempLng = Address1Lng;
                            Address1Lat = Address2Lat;
                            Address1Lng = Address2Lng;
                            Address2Lat = tempLat;
                            Address2Lng = tempLng;

                            Loc1.setLatitude(Address1Lat);
                            Loc1.setLongitude(Address1Lng);
                            Loc2.setLatitude(Address2Lat);
                            Loc2.setLongitude(Address2Lng);

                            distance1 = Loc1.distanceTo(Loc2);
                            distance2 = Loc1.distanceTo(Loc3);

                            if (distance1 > distance2) {
                                tempLat = Address1Lat;
                                tempLng = Address1Lng;
                                Address1Lat = Address2Lat;
                                Address1Lng = Address2Lng;
                                Address2Lat = Address3Lat;
                                Address2Lng = Address3Lng;
                                Address3Lat = tempLat;
                                Address3Lng = tempLng;
                            }
                        } else if (distance2 > distance1 && distance2 > distance3) {
                            tempLat = Address2Lat;
                            tempLng = Address2Lng;
                            Address2Lat = Address1Lat;
                            Address2Lng = Address1Lng;
                            Address1Lat = tempLat;
                            Address1Lng = tempLng;

                            Loc2.setLatitude(Address2Lat);
                            Loc2.setLongitude(Address2Lng);
                            Loc3.setLatitude(Address1Lat);
                            Loc3.setLongitude(Address1Lng);

                            distance1 = Loc2.distanceTo(Loc1);
                            distance2 = Loc1.distanceTo(Loc3);

                            if (distance2 > distance1) {
                                tempLat = Address2Lat;
                                tempLng = Address2Lng;
                                Address2Lat = Address3Lat;
                                Address2Lng = Address3Lng;
                                Address3Lat = tempLat;
                                Address3Lng = tempLng;
                            }
                        }

                        //Add to waypoint list
                        points.add(Point.fromLngLat(Address1Lng, Address1Lat));
                        points.add(Point.fromLngLat(Address2Lng, Address2Lat));
                        points.add(Point.fromLngLat(Address3Lng, Address3Lat));
                        points.add(Point.fromLngLat(DestinationLng, DestinationLat));


                    } else {
                        //Retrieve Eircode Address
                        DriverAddressLat = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Latitude").getValue().toString());
                        DriverAddressLng = Double.valueOf(dataSnapshot.child(LoginEmail).child("Address").child("Longitude").getValue().toString());
                        Address1Lat = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Latitude").getValue().toString());
                        Address1Lng = Double.valueOf(dataSnapshot.child(user1).child("Address").child("Longitude").getValue().toString());
                        Address2Lat = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Latitude").getValue().toString());
                        Address2Lng = Double.valueOf(dataSnapshot.child(user2).child("Address").child("Longitude").getValue().toString());
                        Address3Lat = Double.valueOf(dataSnapshot.child(user3).child("Address").child("Latitude").getValue().toString());
                        Address3Lng = Double.valueOf(dataSnapshot.child(user3).child("Address").child("Longitude").getValue().toString());
                        Address4Lat = Double.valueOf(dataSnapshot.child(user4).child("Address").child("Latitude").getValue().toString());
                        Address4Lng = Double.valueOf(dataSnapshot.child(user4).child("Address").child("Longitude").getValue().toString());

                        Location Loc0 = new Location("");
                        Location Loc1 = new Location("");
                        Location Loc2 = new Location("");
                        Location Loc3 = new Location("");
                        Location Loc4 = new Location("");
                        Loc0.setLatitude(DriverAddressLat);
                        Loc0.setLongitude(DriverAddressLng);
                        Loc1.setLatitude(Address1Lat);
                        Loc1.setLongitude(Address1Lng);
                        Loc2.setLatitude(Address2Lat);
                        Loc2.setLongitude(Address2Lng);
                        Loc3.setLatitude(Address3Lat);
                        Loc3.setLongitude(Address3Lng);
                        Loc4.setLatitude(Address4Lat);
                        Loc4.setLongitude(Address4Lng);


                        Double tempLat, tempLng;
                        float distance1 = Loc0.distanceTo(Loc1);
                        float distance2 = Loc0.distanceTo(Loc2);
                        float distance3 = Loc0.distanceTo(Loc3);
                        float distance4 = Loc0.distanceTo(Loc4);

                        if (distance1 > distance2 && distance1 > distance3 && distance1 > distance4) {
                            tempLat = Address1Lat;
                            tempLng = Address1Lng;
                            Address1Lat = Address2Lat;
                            Address1Lng = Address2Lng;
                            Address2Lat = Address3Lat;
                            Address2Lng = Address3Lng;
                            Address3Lat = Address4Lat;
                            Address3Lng = Address4Lng;
                            Address4Lat = tempLat;
                            Address4Lng = tempLng;

                            Loc1.setLatitude(Address1Lat);
                            Loc1.setLongitude(Address1Lng);
                            Loc2.setLatitude(Address2Lat);
                            Loc2.setLongitude(Address2Lng);
                            Loc3.setLatitude(Address3Lat);
                            Loc3.setLongitude(Address3Lng);

                            distance1 = Loc1.distanceTo(Loc2);
                            distance2 = Loc1.distanceTo(Loc3);
                            distance3 = Loc1.distanceTo(Loc4);

                            if (distance1 > distance2 && distance1 > distance3) {
                                tempLat = Address1Lat;
                                tempLng = Address1Lng;
                                Address1Lat = Address2Lat;
                                Address1Lng = Address2Lng;
                                Address2Lat = Address3Lat;
                                Address2Lng = Address3Lng;
                                Address3Lat = Address4Lat;
                                Address3Lng = Address4Lng;
                                Address4Lat = tempLat;
                                Address4Lng = tempLng;
                            }
                        } else if (distance2 > distance1 && distance2 > distance3 && distance2 > distance4) {
                            tempLat = Address2Lat;
                            tempLng = Address2Lng;
                            Address2Lat = Address3Lat;
                            Address2Lng = Address3Lng;
                            Address3Lat = Address4Lat;
                            Address3Lng = Address4Lng;
                            Address4Lat = tempLat;
                            Address4Lng = tempLng;

                            Loc2.setLatitude(Address2Lat);
                            Loc2.setLongitude(Address2Lng);
                            Loc3.setLatitude(Address3Lat);
                            Loc3.setLongitude(Address3Lng);
                            Loc4.setLatitude(Address4Lat);
                            Loc4.setLongitude(Address4Lng);

                            distance1 = Loc2.distanceTo(Loc3);
                            distance2 = Loc2.distanceTo(Loc4);

                            if (distance2 > distance1) {
                                tempLat = Address2Lat;
                                tempLng = Address2Lng;
                                Address2Lat = Address3Lat;
                                Address2Lng = Address3Lng;
                                Address3Lat = Address4Lat;
                                Address3Lng = Address4Lng;
                                Address4Lat = tempLat;
                                Address4Lng = tempLng;
                            }
                        }

                        //Add to waypoint list
                        points.add(Point.fromLngLat(Address1Lng, Address1Lat));
                        points.add(Point.fromLngLat(Address2Lng, Address2Lat));
                        points.add(Point.fromLngLat(Address3Lng, Address3Lat));
                        points.add(Point.fromLngLat(Address4Lng, Address4Lat));
                        points.add(Point.fromLngLat(DestinationLng, DestinationLat));

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //Setting to navigation view on Mapbox
            setContentView(R.layout.activity_navigation);
            navigationView = findViewById(R.id.navigationView);
            navigationView.onCreate(savedInstanceState);
            navigationView.getNavigationAsync(this);

        }
        else{
            Toast.makeText(getApplicationContext(), "Please enable location permission", Toast.LENGTH_LONG).show();
            //.putExtra allows to send the LoginEmail
            Intent intent = new Intent(DirectionsActivity.this, OpenChatRoomMessengerActivity.class)
                    .putExtra("LoginEmail", LoginEmail);
            startActivity(intent);
            finish();
        }
    }


    private NavigationViewOptions setupOptions(Point origin) {
        dropoffDialogShown = false;

        //setting the navigation route
        NavigationViewOptions.Builder options = NavigationViewOptions.builder();
        options.navigationListener(this);
        options.progressChangeListener(this);
        options.origin(Point.fromLngLat(DriverAddressLng, DriverAddressLat));
        //remove the waypoint at point 0 (first waypoint)
        options.destination(points.remove(0));
        options.shouldSimulateRoute(false);
        return options.build();
    }

    @Override
    public void onNavigationReady() {
        navigationView.startNavigation(setupOptions(points.remove(0)));
    }

    @Override
    public void onProgressChange(Location location, RouteProgress routeProgress) {
        if (RouteUtils.isArrivalEvent(routeProgress)) {
            //Tracks the drivers GPS
            lastKnownLocation = location;

            //Error handler
            if (!dropoffDialogShown && !points.isEmpty()) {
                showDropoffDialog();
                dropoffDialogShown = true;
            }
        }
    }

    private void showDropoffDialog() {

        //Alert Dialog set to a alert when approaching a waypoint
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.dropoff_dialog_text));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dropoff_dialog_positive_text),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int in) {
                        navigationView.startNavigation(
                                setupOptions(Point.fromLngLat(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude())));
                    }
                });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.dropoff_dialog_negative_text),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int in) {

                    }
                });

        alertDialog.show();
    }

    //Standard Navigation API methods which need to be implemented
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        navigationView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        if (!navigationView.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        navigationView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        navigationView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        navigationView.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onCancelNavigation() {
        finish();
    }

    @Override
    public void onNavigationFinished() {

    }

    @Override
    public void onNavigationRunning() {

    }
}
