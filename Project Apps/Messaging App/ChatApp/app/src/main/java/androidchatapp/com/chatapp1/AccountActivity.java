package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidchatapp.com.chatapp1.R;

public class AccountActivity extends AppCompatActivity {


    private TextView
            Full_Name, Email, AddressLine1,
            AddressLine2, AddressTown, AddressCounty, AddressCountry, AddressEircode,
            Phone_Number, Password;
    Button ReturnMainMenu;

    //Connection to the firebase database
    private DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);


        //Retrieving the Email from Login activity for setting username
        Intent RetrieveIntent = getIntent();
        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");

        //Assigning variables to objects
        Full_Name = findViewById(R.id.Full_Name);
        Email = findViewById(R.id.Email);
        AddressLine1 = findViewById(R.id.AddressLine1);
        AddressLine2 = findViewById(R.id.AddressLine2);
        AddressTown = findViewById(R.id.AddressTown);
        AddressCounty = findViewById(R.id.AddressCounty);
        AddressCountry = findViewById(R.id.AddressCountry);
        AddressEircode = findViewById(R.id.AddressEircode);
        Phone_Number = findViewById(R.id.Phone_Number);
        Password = findViewById(R.id.Password);
        ReturnMainMenu = findViewById(R.id.ReturnMainMenuAccount);

        ReturnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //.putExtra allows to send the LoginEmail
                Intent intent = new Intent(AccountActivity.this, MenuActivity.class)
                        .putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
                finish();
            }
        });

        //Retrieve data from the database
        root.child("Users").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Splitting the email string so it can be used to access the DB at a later stage
                String[] Email_Part_1 = LoginEmail.split("@");

                //Retrieving from firebase and setting the TextViews
                Full_Name.setText("Full Name: " + dataSnapshot.child(Email_Part_1[0]).child("Full_Name").getValue().toString());
                Email.setText("Email: " + dataSnapshot.child(Email_Part_1[0]).child("Email_Address").getValue().toString());
                Phone_Number.setText("Phone Number: " + dataSnapshot.child(Email_Part_1[0]).child("Phone_Number").getValue().toString());
                AddressLine1.setText("Address Line 1: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressLine1").getValue().toString());
                AddressLine2.setText("Address Line 2: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressLine2").getValue().toString());
                AddressTown.setText("Address Town: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressTown").getValue().toString());
                AddressCounty.setText("Address County: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressCounty").getValue().toString());
                AddressCountry.setText("Address Country: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressCountry").getValue().toString());
                AddressEircode.setText("Address Eircode: " + dataSnapshot.child(Email_Part_1[0]).child("Address").child("AddressEircode").getValue().toString());
                Password.setText("Password: " + dataSnapshot.child(Email_Part_1[0]).child("Password").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}