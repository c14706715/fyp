package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/*
    Jake Young
    C14706715
    DT282/4
*/

public class Chat_Room  extends AppCompatActivity{

    //Declaring objects needed
    Button Send_Btn;
    String Room_Name, Chat_Message, Chat_LoginEmail;
    private EditText Entered_Message;
    private TextView Chat;
    String LoginEmail;
    private DatabaseReference root;
    private String key;
    Button Im_Driving_Btn, Request_MyTaxi_Btn, Add_People;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_room);

        //Assigning values to the objects
        Send_Btn = findViewById(R.id.btn_send);
        Entered_Message = findViewById(R.id.msg_input);
        Chat = findViewById(R.id.textView);
        Im_Driving_Btn = findViewById(R.id.Im_Driving);
        Request_MyTaxi_Btn = findViewById(R.id.Request_MyTaxi);
        Add_People = findViewById(R.id.AddPeople);

        //Retrieve these variables from other activities
        LoginEmail = getIntent().getExtras().get("LoginEmail").toString();
        Room_Name = getIntent().getExtras().get("Room_Name").toString();

        //Error checking
        //Log.i("CR-Chat Room Email", " "+LoginEmail);

        //Title for chat
        setTitle(Room_Name);

        //Getting data from the Firebase database
        root = FirebaseDatabase.getInstance().getReference().child("Group_Chats").child(Room_Name);

        //Button to add people
        Add_People.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Chat_Room.this, PeopleListActivity.class)
                        .putExtra("LoginEmail", LoginEmail)
                        .putExtra("Room_Name", Room_Name));
            }
        });

        //Get directed to the Driving page
        Im_Driving_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Chat_Room.this, DrivingActivity.class)
                        .putExtra("Room_Name", Room_Name)
                        .putExtra("LoginEmail", LoginEmail));
            }
        });

        //Get directed to the MyTaxi page
        Request_MyTaxi_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://us.mytaxi.com/order-taxi.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        Chat.setText("");


        Send_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Sending data to the database
                Map<String,Object> map = new HashMap<>();
                key = root.push().getKey();
                root.updateChildren(map);

                DatabaseReference message_root = root.child(key);
                Map<String,Object> map2 = new HashMap<>();
                map2.put("LoginEmail", LoginEmail);
                map2.put("Message", Entered_Message.getText().toString());



                message_root.updateChildren(map2);
            }
        });

        //Necessary methods for access to DB
        root.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Iterator i = dataSnapshot.getChildren().iterator();

                while (i.hasNext()){
                    //Retrieving data from the database
                    Chat_Message = (String) ((DataSnapshot)i.next()).getValue();
                    Chat_LoginEmail = (String) ((DataSnapshot)i.next()).getValue();

                    Chat.append(Chat_Message + " : "+Chat_LoginEmail +"\n");
                    Entered_Message.setText("");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Iterator i = dataSnapshot.getChildren().iterator();

                while (i.hasNext()){
                    //Retrieving data from the database
                    Chat_Message = (String) ((DataSnapshot)i.next()).getValue();
                    Chat_LoginEmail = (String) ((DataSnapshot)i.next()).getValue();

                    Chat.append(Chat_Message + " : "+Chat_LoginEmail +"\n");
                    Entered_Message.setText("");
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
