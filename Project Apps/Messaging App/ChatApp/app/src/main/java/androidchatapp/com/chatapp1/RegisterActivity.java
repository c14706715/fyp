package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.List;


/**
 * Created by Jake
 * * */

public class RegisterActivity extends Activity {

    //Declared the necessary variables
    private String AddressCountry = "Ireland";
    private EditText EnteredFullName, EnteredEmail,
            EnteredPassword, EnteredAddressLine1, EnteredAddressLine2,
            EnteredAddressTown, EnteredAddressCounty, EnteredAddressEircode, EnteredPhoneNumber;
    Button RegisterBtn, LoginBtn, EircodeHelpBtn;
    private FirebaseAuth auth;
    String EmailValue;


    //Used to encode the address into Latitude and Longitude so that google Map Directions can
    //read the co-ordinates
    public void Geocode_LatLng(String Name, String Full_Address) throws IOException {

        String Geocode_Address = Full_Address;
        String EmailValue = Name;

        Geocoder geocoder = new Geocoder(this);


        //Initialising the references for the the firebase database
        FirebaseDatabase RootReference = FirebaseDatabase.getInstance();

        //Gets reference to the Users database
        final DatabaseReference UserReference = RootReference.getReference("Users");

        //API function to get lat and lng
        List<android.location.Address> list = geocoder.getFromLocationName(Geocode_Address, 1);

        //put the first value in the array into Address
        Address address = list.get(0);

        //Retrieve the values from Address
        double lat = address.getLatitude();
        double lng = address.getLongitude();

        //Push the values to the users database
        UserReference.child(EmailValue).child("Address").child("Latitude").setValue(lat);
        UserReference.child(EmailValue).child("Address").child("Longitude").setValue(lng);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        //Retrieve the firebase instance
        auth = FirebaseAuth.getInstance();

        //Assigning variables to objects
        RegisterBtn = findViewById(R.id.RegisterBtn);
        LoginBtn = findViewById(R.id.LoginBtn);
        EircodeHelpBtn = findViewById(R.id.EircodeHelpBtn);
        EnteredFullName = findViewById(R.id.EnteredFullName);
        EnteredEmail = findViewById(R.id.EnteredEmail);
        EnteredAddressLine1 = findViewById(R.id.EnteredAddressLine1);
        EnteredAddressLine2 = findViewById(R.id.EnteredAddressLine2);
        EnteredAddressTown = findViewById(R.id.EnteredAddressTown);
        EnteredAddressCounty = findViewById(R.id.EnteredAddressCounty);
        EnteredAddressEircode = findViewById(R.id.EnteredAddressEircode);
        EnteredPhoneNumber = findViewById(R.id.EnteredPhoneNumber);
        EnteredPassword = findViewById(R.id.EnteredPassword);


        //To return to the login page
        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

            }
        });

        //Initialising the references for the the firebase database
        FirebaseDatabase RootReference = FirebaseDatabase.getInstance();

        //Gets reference to the Users database
        final DatabaseReference UserReference = RootReference.getReference("Users");

        EircodeHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open the Irish find Eircode webpage for user to retrieve eircode
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://finder.eircode.ie/#/"));
                startActivity(intent);
            }
        });

        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Retrieve the users entered details
                String email = EnteredEmail.getText().toString().trim().toLowerCase();
                String password = EnteredPassword.getText().toString().trim();
                String Full_Name = EnteredFullName.getText().toString().trim();
                String Phone_Number = EnteredPhoneNumber.getText().toString().trim();
                String AddressLine1 = EnteredAddressLine1.getText().toString().trim();
                String AddressLine2 = EnteredAddressLine2.getText().toString().trim();
                String AddressTown = EnteredAddressTown.getText().toString().trim();
                String AddressCounty = EnteredAddressCounty.getText().toString().trim();
                String AddressEircode = EnteredAddressEircode.getText().toString().trim();


                String[] Email_Part_1 = email.split("@");
                EmailValue = Email_Part_1[0];

                //Pushing the users registration details to the firebase database
                UserReference.child(EmailValue).child("Full_Name").setValue(Full_Name);
                UserReference.child(EmailValue).child("Email_Address").setValue(email);
                UserReference.child(EmailValue).child("Phone_Number").setValue(Phone_Number);
                UserReference.child(EmailValue).child("Address").child("AddressLine1").setValue(AddressLine1);
                UserReference.child(EmailValue).child("Address").child("AddressLine2").setValue(AddressLine2);
                UserReference.child(EmailValue).child("Address").child("AddressTown").setValue(AddressTown);
                UserReference.child(EmailValue).child("Address").child("AddressCounty").setValue(AddressCounty);
                UserReference.child(EmailValue).child("Address").child("AddressCountry").setValue(AddressCountry);
                UserReference.child(EmailValue).child("Address").child("AddressEircode").setValue(AddressEircode);
                UserReference.child(EmailValue).child("Password").setValue(password);

                //In a try catch due to IO Exception
                try {
                    //Call the geocode method with the variables returned to the function
                    Geocode_LatLng(EmailValue, AddressEircode);
                } catch (IOException e) {
                    e.printStackTrace();
                }
               
                //Check if all fields are completed, if not a message will appear
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(Full_Name)) {
                    Toast.makeText(getApplicationContext(), "Enter full name please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(Phone_Number)) {
                    Toast.makeText(getApplicationContext(), "Enter phone number please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(AddressLine1)) {
                    Toast.makeText(getApplicationContext(), "Enter address line 1 please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(AddressLine2)) {
                    Toast.makeText(getApplicationContext(), "Enter address line 2 please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(AddressTown)) {
                    Toast.makeText(getApplicationContext(), "Enter town please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(AddressEircode)) {
                    Toast.makeText(getApplicationContext(), "Enter Eircode please\nFor help please click on the help button", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(AddressCounty)) {
                    Toast.makeText(getApplicationContext(), "Enter county please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password please", Toast.LENGTH_SHORT).show();
                    return;
                }

                //checks if the registration was successful and outputs a message according to the result
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (!task.isSuccessful()) {
                                    Toast.makeText(RegisterActivity.this, "Registration Failed\n" + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                    finish();
                                }
                            }
                        });
            }
        });
    }
}
