package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


/**
 * Created by Jake on 12/12/2017.
 */

public class LoginActivity extends AppCompatActivity {

    //Declaring variables
    private FirebaseAuth auth;
    private Button LoginBtn, RegisterBtn;
    private EditText EnteredEmail, EnteredPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        //Initializing variables
        EnteredEmail = findViewById(R.id.EnteredEmail);
        EnteredPassword = findViewById(R.id.EnteredPassword);
        LoginBtn = findViewById(R.id.LoginBtn);
        RegisterBtn = findViewById(R.id.RegisterBtn);

        //retrieve the Firebase instance
        auth = FirebaseAuth.getInstance();


        /*
        //Checks if the instance is not null
        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
            finish();
        }
        */

        //Get directed to the Registration page
        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        //OnClick Login, checks if Text field are correctly inputted and are in the database,
        //If so you will be directed to the main menu page
        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //checks if text fields are empty, if so display message to enter credentials
                final String email = EnteredEmail.getText().toString();
                final String password = EnteredPassword.getText().toString();
                String alphaNumeric ="[a-zA-Z0-9]+";

                //Checks if fields are empty
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address please", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    if(password.matches(alphaNumeric)){
                        Toast.makeText(getApplicationContext(), "Enter password please", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    //check is password is alphanumeric
                    else{
                        Toast.makeText(getApplicationContext(), "Please enter a password which is alphanumeric", Toast.LENGTH_SHORT).show();
                    }

                }

                //double check the login fields
                if(email.length() > 2 && password.length() > 2) {
                    //Sign into the irebase Database
                    auth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        if (password.length() < 8) {
                                            EnteredPassword.setError(getString(R.string.minPassword));
                                        }
                                        Toast.makeText(LoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    } else {
                                        Intent intent = new Intent(LoginActivity.this, MenuActivity.class)
                                                .putExtra("LoginEmail", EnteredEmail.getText().toString());

                                        startActivity(intent);

                                        finish();
                                    }
                                }
                            });
                }
                else{
                    Toast.makeText(getApplicationContext(), "Please fill in the required fields:\nEmail Address\nPassword", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}