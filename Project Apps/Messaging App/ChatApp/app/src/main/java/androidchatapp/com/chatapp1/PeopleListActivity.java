package androidchatapp.com.chatapp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PeopleListActivity extends AppCompatActivity {

    //Declaring variables
    ListView PeopleListView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> People_List = new ArrayList<>();
    String FinalClickedUserEmailString, FCUEMS = " ", EmailValue, Room_Name_2;
    ArrayList <String> List_Of_Users = new ArrayList<>();
    Button Add_People_Btn, Return_To_Menu_Btn;
    String ListUsers, Security;
    int checker = 1;

    //Gets reference to the Users database
    DatabaseReference root = FirebaseDatabase.getInstance().getReference().getRoot();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Switch
        // This can be done by pressing the arrow in the top right hand corner
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(PeopleListActivity.this, Chat_Room.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_list);

        //Retrieving the Email from Login activity for setting username
        Intent RetrieveIntent = getIntent();
        final String LoginEmail = RetrieveIntent.getStringExtra("LoginEmail");
        final String Room_Name = RetrieveIntent.getStringExtra("Room_Name");
        final String[] ClickedUserEmailString = new String[1];
        final String[] FinalListOfUsers = new String[1];
        final String[] FinalSecurity = new String[1];

        Add_People_Btn = findViewById(R.id.add_People_Btn);
        Return_To_Menu_Btn = findViewById(R.id.Return_To_Menu);

        Return_To_Menu_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PeopleListActivity.this, MenuActivity.class).putExtra("LoginEmail", LoginEmail);
                startActivity(intent);
            }
        });

        root.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FinalSecurity[0] = dataSnapshot.child("Group_Chats").child(Room_Name).child("Security_Code").getValue().toString();

                Security = FinalSecurity[0];
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Add_People_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Filling in the fields
                String To = FCUEMS;
                String Subject = "Invitation to join group chat: "+Room_Name;
                String Email =
                        "Hi," +
                                "\n" +
                                "\nYou have been invited to join the group chat\n" +
                                "Called " + Room_Name + "\n" +
                                "Below is the security code\n" +
                                "Use this code to access the group chat when prompted\n" +
                                "\n" +
                                "Security Code: " + Security +
                                "\n\n" +
                                "Thanks," +
                                "\n" +
                                "KiddieCarpool Team";

                //Sending an email to user to join the group chat
                //Filling the email fields
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ To});
                intent.putExtra(Intent.EXTRA_SUBJECT, Subject);
                intent.putExtra(Intent.EXTRA_TEXT, Email);

                //Gets the email client
                intent.setType("message/rfc822");

                //Error Checking
                //Log.i("PL - Room ",""+Room_Name);
                //Log.i("PL - ListOfUser ",""+List_Of_Users.toString());

                //Sending data to the database - List of users needed for maps
                root.child("Group_Chats").child(Room_Name).child("List_Of_Users").setValue(List_Of_Users.toString());

                root.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        FinalListOfUsers[0] = dataSnapshot.child("Group_Chats").child(Room_Name).child("List_Of_Users").getValue().toString();

                        ListUsers = FinalListOfUsers[0];
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //Choosing provider i.e. gmail, hotmail etc.
                startActivity(Intent.createChooser(intent, "Choose an email provider"));
            }
        });


        PeopleListView = findViewById(R.id.PeopleListView);

        //Setting the standard list layout
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, People_List);

        //Attaching to the view
        PeopleListView.setAdapter(arrayAdapter);


        //Retrieve data from the database
        root.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                Set<String> set = new HashSet<>();
                Iterator i = dataSnapshot.getChildren().iterator();

                //Error checking
                //Log.i("People List" , ""+LoginEmail);
                String[] Email_Part_1 = LoginEmail.split("@");


                while (i.hasNext()) {
                    //Log.i("PA Email", " "+Email_Part_1[0]);

                    set.add(((DataSnapshot) i.next()).getKey());

                    //Removes the main user
                    set.remove(Email_Part_1[0]);

                    //Count the total views
                    checker ++;

                    //error checking
                    //Log.i("i.next", " "+ dataSnapshot.getValue().toString());
                }

                People_List.clear();
                People_List.addAll(set);

                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        //Onclick a room name it will open the messenger
        PeopleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //get the users email in order to access the users information in order to retrieve their email
                final String ClickedUserEmail = ((TextView) view).getText().toString();

                //Splitting the email string so it can be used to access the DB at a later stage
                String[] Email_Part_1 = ClickedUserEmail.split("@");
                EmailValue = Email_Part_1[0];

               //Add users to the list provided not already added
                if (!List_Of_Users.contains(EmailValue)) {
                    List_Of_Users.add(EmailValue);
                }

                if(List_Of_Users.size() == 4){
                    Toast.makeText(getApplicationContext(), "Sorry only a max of three users can be added to the group", Toast.LENGTH_LONG).show();

                    //Resetting all the selected names to unselected
                    for(int j=0; j<checker; j++){
                        PeopleListView.setItemChecked(j, false);
                    }

                    List_Of_Users.clear();
                }


                //Error Check
                //Log.i("PL-Clicked Email-Part 0", "" + EmailValue);
                //Log.i("PL-Clicked List emails", "" + List_Of_Users.toString());

                //Retrieve data from the database
                root.child("Users").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (int j = 0; j < List_Of_Users.size(); j++) {

                            //Retrieving the user email from database
                            ClickedUserEmailString[0] = dataSnapshot.child(EmailValue).child("Email_Address").getValue().toString();

                            //Converting the array value to a String
                            FinalClickedUserEmailString = ClickedUserEmailString[0];

                            //Check is email is not already entered if not add with semi-colon
                            //to split to send as seperate emails
                            if (!FCUEMS.contains(FinalClickedUserEmailString)) {
                                FCUEMS = FCUEMS + FinalClickedUserEmailString + ",";
                            }
                            //Error Check
                            //Log.i("PL-Clicked User Email 1", "" + ClickedUserEmailString[0]);
                            //Error Check
                            //Log.i("PL-Clicked User Email 2", "" + FinalClickedUserEmailString);
                            //Error Check
                            //this holds the usernames which will be used to geth thew users Lat Lng
                            //Log.i("PL-Clicked List 2", "" + List_Of_Users.toString());
                        }
                        //Error Check
                        //Log.i("PL-Clicked User Email 2", "" + FCUEMS);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });
    }
}
