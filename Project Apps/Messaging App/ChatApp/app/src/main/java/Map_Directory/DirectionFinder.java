package Modules;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DirectionFinder {

    //Declare variables
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    private static final String GOOGLE_API_KEY = "AIzaSyCbedbT6nGzyoUhe99EsVSateMCKxyuOVA";
    private DirectionFinderListener listener;
    private List<String> list = new ArrayList<>();
    String Point3 = "Dundrum Shopping Centre";
    String DFFullUsersEircode;

    public DirectionFinder(DirectionFinderListener listener, String FullUsersEircode) {
        //Setting to the variable in the method
        this.listener = listener;
        this.DFFullUsersEircode = FullUsersEircode;

        //Error Checking
        Log.i("DF: Full Users ", "" + DFFullUsersEircode);

        //Spliiting the incoming String into the seperate Eircodes
        list = new ArrayList<>(Arrays.asList(DFFullUsersEircode.split(", ")));

        //Error Code
        for(int i=0; i<list.size(); i++) {
            Log.i("DF: List " + i, "" + list.get(i));
        }
    }

    //Standar method throws exception when trying the CreateUrl Method
    public void execute() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createUrl());
    }


    private String createUrl() throws UnsupportedEncodingException {
        String urlPoint1 = URLEncoder.encode(list.get(0), "utf-8");
        String urlPoint2 = URLEncoder.encode(list.get(1), "utf-8");
        String urlPoint3 = URLEncoder.encode(Point3, "utf-8");

        //Starting point, destination and middle pick up points
        //Organize waypoints according to best route (Efficiency)
        return DIRECTION_URL_API +
                "origin=" + urlPoint1 +
                "&destination=" + urlPoint2 +
                "&waypoints=optimize:true|" + urlPoint3 +
                "&key=" + GOOGLE_API_KEY;
    }

    //Standard method
    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

        List<Route> routes = new ArrayList<>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");

        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            //Retrieve the JSON and parse the info needed
            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

            //Send the parsed info to the Route method
            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.points = decodePolyLine(overview_polylineJson.getString("points"));
            //route.order = jsonOrder.getString("waypoint_order");

            //Added values to the Route method
            routes.add(route);
        }

        listener.onDirectionFinderSuccess(routes);
    }


    //Standard method to decode the retrieved polylines
    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }
}
