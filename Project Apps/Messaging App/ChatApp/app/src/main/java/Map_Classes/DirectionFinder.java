package Map_Classes;

import android.content.Intent;
import android.os.AsyncTask;
//import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class DirectionFinder {
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    private static final String GOOGLE_API_KEY = "AIzaSyDnwLF2-WfK8cVZt9OoDYJ9Y8kspXhEHfI";
    private DirectionFinderListener listener;
    private String origin;
    private String destination;
    private String waypoint1, waypoint2, waypoint3, waypoint4;

    public DirectionFinder(DirectionFinderListener listener, String origin, String destination, String waypoint1, String waypoint2, String waypoint3, String waypoint4) {
        this.listener = listener;
        this.origin = origin;
        this.destination = destination;
        this.waypoint1= waypoint1;
        this.waypoint2 = waypoint2;
        this.waypoint3 = waypoint3;
        this.waypoint4 = waypoint4;
    }

    public void execute() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createUrl());
    }

    private String createUrl() throws UnsupportedEncodingException {

        String urlOrigin = URLEncoder.encode(origin, "utf-8");
        String urlDestination = destination;
        String urlWaypoint1 = URLEncoder.encode(waypoint1, "utf-8");
        String urlWaypoint2 = URLEncoder.encode(waypoint2, "utf-8");
        String urlWaypoint3 = URLEncoder.encode(waypoint3, "utf-8");
        String urlWaypoint4 = URLEncoder.encode(waypoint4, "utf-8");

        int URLW1_Index = waypoint1.length();
        int URLW2_Index = waypoint2.length();
        int URLW3_Index = waypoint3.length();
        int URLW4_Index = waypoint4.length();

        //Log.i("WP1 "+urlWaypoint1, " len "+URLW1_Index);
        //Log.i("WP2 "+urlWaypoint2, " len "+URLW2_Index);
        //Log.i("WP3 "+urlWaypoint3, " len "+URLW3_Index);
        //Log.i("WP4 "+urlWaypoint4, " len "+URLW4_Index);

        if(URLW1_Index == 1){
            //Log.i("Direction Finder ", " "+DIRECTION_URL_API + "origin=" + urlOrigin +
              //      "&destination=" + urlDestination +
                //    "&key=" + GOOGLE_API_KEY);
            return DIRECTION_URL_API +
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination  +
                    "&key=" + GOOGLE_API_KEY;
        }
        else if(URLW2_Index == 1){
            //Log.i("Direction Finder ", " "+DIRECTION_URL_API+
             //       "origin=" + urlOrigin +
              //      "&destination=" + urlDestination +
               //     "&" + "waypoints=" + urlWaypoint1 +
                //    "&key=" + GOOGLE_API_KEY);
            return DIRECTION_URL_API +
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +
                    "&key=" + GOOGLE_API_KEY;
        }
        else if(URLW3_Index == 1){
            /*Log.i("Direction Finder ", " "+DIRECTION_URL_API+
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +"|" + urlWaypoint2 +
                    "&key=" + GOOGLE_API_KEY);
            */return DIRECTION_URL_API +
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +
                    "|" + urlWaypoint2 +
                    "&key=" + GOOGLE_API_KEY;
        }
        else if(URLW4_Index == 1){
            /*Log.i("Direction Finder ", " "+DIRECTION_URL_API+
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +"|" + urlWaypoint2 +
                    "|" + urlWaypoint3 + "&key=" + GOOGLE_API_KEY);
           */ return DIRECTION_URL_API +
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +
                    "|" + urlWaypoint2 +
                    "|" + urlWaypoint3 +
                    "&key=" + GOOGLE_API_KEY;
        }
        else {
           /* Log.i("Direction Finder ", " "+DIRECTION_URL_API+
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +"|" + urlWaypoint2 +
                    "|" + urlWaypoint3 + "|" + urlWaypoint4 + "&key=" + GOOGLE_API_KEY);
            */return DIRECTION_URL_API +
                    "origin=" + urlOrigin +
                    "&destination=" + urlDestination +
                    "&" + "waypoints=" + urlWaypoint1 +
                    "|" + urlWaypoint2 +
                    "|" + urlWaypoint3 +
                    "|" + urlWaypoint4 +
                    "&key=" + GOOGLE_API_KEY;
        }
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

        List<Route> routes = new ArrayList<>();

        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();



            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");

            if(jsonLegs.length() == 1){
                //retrieving the data from each stop
                JSONObject jsonLeg0 = jsonLegs.getJSONObject(0);

                //retrieving the distance specific data from each leg
                JSONObject jsonDistance0 = jsonLeg0.getJSONObject("distance");

                //retrieving the duration specific data from each leg
                JSONObject jsonDuration0 = jsonLeg0.getJSONObject("duration");

                JSONObject jsonDirection0 = jsonLeg0.getJSONObject("html_instructions");


                //Error Checking
                //Log.i("Length", ""+jsonLegs.length());
                //Log.i("Array 0", " "+jsonLeg0);

                //getting the total distance (in metres)
                int TotalDistance = jsonDistance0.getInt("value");

                //getting the total duration (in seconds)
                int TotalDuration = jsonDuration0.getInt("value");

                String str = jsonDirection0.toString();

                JSONObject jsonEndLocation = jsonLeg0.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg0.getJSONObject("start_location");

                //Convert to KM
                TotalDistance = TotalDistance/1000;

                //Convert to hrs and mins
                int Hours = TotalDuration/3600;
                int Minutes = (TotalDuration/60) - (Hours*60);

                //Error Checking
                //Log.i("Total Duration: "+TotalDuration, "Total Distance: "+TotalDistance);

                //Send to Route, Distance and Duration Methods
                route.distance = new Distance(TotalDistance + " KM", TotalDistance);
                route.duration = new Duration(Hours + " Hrs " + Minutes + " Mins", TotalDuration);
                route.endAddress = jsonLeg0.getString("end_address");
                route.startAddress = jsonLeg0.getString("start_address");
                route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                route.points = decodePolyLine(overview_polylineJson.getString("points"));
                //route.directions = jsonLeg0.getString("html_instructions");

                routes.add(route);
            }
            else if(jsonLegs.length() == 2){
                //retrieving the data from each stop
                JSONObject jsonLeg0 = jsonLegs.getJSONObject(0);
                JSONObject jsonLeg1 = jsonLegs.getJSONObject(1);

                //retrieving the distance specific data from each leg
                JSONObject jsonDistance0 = jsonLeg0.getJSONObject("distance");
                JSONObject jsonDistance1 = jsonLeg1.getJSONObject("distance");

                //retrieving the duration specific data from each leg
                JSONObject jsonDuration0 = jsonLeg0.getJSONObject("duration");
                JSONObject jsonDuration1 = jsonLeg1.getJSONObject("duration");

                JSONObject jsonEndLocation = jsonLeg1.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg0.getJSONObject("start_location");

                //Error Checking
                //Log.i("Length", ""+jsonLegs.length());
                //Log.i("Array 0", " "+jsonLeg0);
                //Log.i("Array 1", " "+jsonLeg1);

                //getting the total distance (in metres)
                int TotalDistance =
                        jsonDistance0.getInt("value") +
                        jsonDistance1.getInt("value");

                //getting the total duration (in seconds)
                int TotalDuration =
                        jsonDuration0.getInt("value") +
                        jsonDuration1.getInt("value");

                //Convert to KM
                TotalDistance = TotalDistance/1000;

                //Convert to hrs and mins
                int Hours = TotalDuration/3600;
                int Minutes = (TotalDuration/60) - (Hours*60);

                //Error Checking
                //Log.i("Total Duration: "+TotalDuration, "Total Distance: "+TotalDistance);

                //Send to Route, Distance and Duration Methods
                route.distance = new Distance(TotalDistance + " KM", TotalDistance);
                route.duration = new Duration(Hours + " Hrs " + Minutes + " Mins", TotalDuration);
                route.endAddress = jsonLeg1.getString("end_address");
                route.startAddress = jsonLeg0.getString("start_address");
                route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                route.points = decodePolyLine(overview_polylineJson.getString("points"));
                /*route.directions =
                        jsonLeg0.getString("html_instructions") +
                        jsonLeg1.getString("html_instructions");*/

                routes.add(route);
            }
            else if(jsonLegs.length() == 3){
                //retrieving the data from each stop
                JSONObject jsonLeg0 = jsonLegs.getJSONObject(0);
                JSONObject jsonLeg1 = jsonLegs.getJSONObject(1);
                JSONObject jsonLeg2 = jsonLegs.getJSONObject(2);

                //retrieving the distance specific data from each leg
                JSONObject jsonDistance0 = jsonLeg0.getJSONObject("distance");
                JSONObject jsonDistance1 = jsonLeg1.getJSONObject("distance");
                JSONObject jsonDistance2 = jsonLeg2.getJSONObject("distance");

                //retrieving the duration specific data from each leg
                JSONObject jsonDuration0 = jsonLeg0.getJSONObject("duration");
                JSONObject jsonDuration1 = jsonLeg1.getJSONObject("duration");
                JSONObject jsonDuration2 = jsonLeg2.getJSONObject("duration");


                JSONObject jsonEndLocation = jsonLeg2.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg0.getJSONObject("start_location");

                //Error Checking
                //Log.i("Length", ""+jsonLegs.length());
                //Log.i("Array 0", " "+jsonLeg0);
                //Log.i("Array 1", " "+jsonLeg1);
                //Log.i("Array 2", " "+jsonLeg2);

                //getting the total distance (in metres)
                int TotalDistance =
                        jsonDistance0.getInt("value") +
                        jsonDistance1.getInt("value") +
                        jsonDistance2.getInt("value");

                //getting the total duration (in seconds)
                int TotalDuration =
                        jsonDuration0.getInt("value") +
                        jsonDuration1.getInt("value") +
                        jsonDuration2.getInt("value");

                //Convert to KM
                TotalDistance = TotalDistance/1000;

                //Convert to hrs and mins
                int Hours = TotalDuration/3600;
                int Minutes = (TotalDuration/60) - (Hours*60);


                //Error Checking
                //Log.i("Total Duration: "+TotalDuration, "Total Distance: "+TotalDistance);

                //Send to Route, Distance and Duration Methods
                route.distance = new Distance(TotalDistance + " KM", TotalDistance);
                route.duration = new Duration(Hours + " Hrs " + Minutes + " Mins", TotalDuration);
                route.endAddress = jsonLeg2.getString("end_address");
                route.startAddress = jsonLeg0.getString("start_address");
                route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                route.points = decodePolyLine(overview_polylineJson.getString("points"));
                /*route.directions =
                        jsonLeg0.getString("html_instructions") +
                        jsonLeg1.getString("html_instructions") +
                        jsonLeg2.getString("html_instructions");*/

                routes.add(route);
            }
            else if(jsonLegs.length() == 4){
                //retrieving the data from each stop
                JSONObject jsonLeg0 = jsonLegs.getJSONObject(0);
                JSONObject jsonLeg1 = jsonLegs.getJSONObject(1);
                JSONObject jsonLeg2 = jsonLegs.getJSONObject(2);
                JSONObject jsonLeg3 = jsonLegs.getJSONObject(3);

                //retrieving the distance specific data from each leg
                JSONObject jsonDistance0 = jsonLeg0.getJSONObject("distance");
                JSONObject jsonDistance1 = jsonLeg1.getJSONObject("distance");
                JSONObject jsonDistance2 = jsonLeg2.getJSONObject("distance");
                JSONObject jsonDistance3 = jsonLeg3.getJSONObject("distance");

                //retrieving the duration specific data from each leg
                JSONObject jsonDuration0 = jsonLeg0.getJSONObject("duration");
                JSONObject jsonDuration1 = jsonLeg1.getJSONObject("duration");
                JSONObject jsonDuration2 = jsonLeg2.getJSONObject("duration");
                JSONObject jsonDuration3 = jsonLeg3.getJSONObject("duration");

                JSONObject jsonEndLocation = jsonLeg3.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg0.getJSONObject("start_location");

                //Error Checking
                //Log.i("Length", ""+jsonLegs.length());
                //Log.i("Array 0", " "+jsonLeg0);
                //Log.i("Array 1", " "+jsonLeg1);
                //Log.i("Array 2", " "+jsonLeg2);
                //Log.i("Array 3", " "+jsonLeg3);

                //getting the total distance (in metres)
                int TotalDistance =
                        jsonDistance0.getInt("value") +
                        jsonDistance1.getInt("value") +
                        jsonDistance2.getInt("value") +
                        jsonDistance3.getInt("value");

                //getting the total duration (in seconds)
                int TotalDuration =
                        jsonDuration0.getInt("value") +
                        jsonDuration1.getInt("value") +
                        jsonDuration2.getInt("value") +
                        jsonDuration3.getInt("value");

                //Convert to KM
                TotalDistance = TotalDistance/1000;

                //Convert to hrs and mins
                int Hours = TotalDuration/3600;
                int Minutes = (TotalDuration/60) - (Hours*60);

                //Error Checking
                //Log.i("Total Duration: "+TotalDuration, "Total Distance: "+TotalDistance);

                //Send to Route, Distance and Duration Methods
                route.distance = new Distance(TotalDistance + " KM", TotalDistance);
                route.duration = new Duration(Hours + " Hrs " + Minutes + " Mins", TotalDuration);
                route.endAddress = jsonLeg3.getString("end_address");
                route.startAddress = jsonLeg0.getString("start_address");
                route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                route.points = decodePolyLine(overview_polylineJson.getString("points"));
                /*route.directions =
                        jsonLeg0.getString("html_instructions") +
                        jsonLeg1.getString("html_instructions") +
                        jsonLeg2.getString("html_instructions") +
                        jsonLeg3.getString("html_instructions");
                routes.add(route);*/
            }
            else {

                //retrieving the data from each stop
                JSONObject jsonLeg0 = jsonLegs.getJSONObject(0);
                JSONObject jsonLeg1 = jsonLegs.getJSONObject(1);
                JSONObject jsonLeg2 = jsonLegs.getJSONObject(2);
                JSONObject jsonLeg3 = jsonLegs.getJSONObject(3);
                JSONObject jsonLeg4 = jsonLegs.getJSONObject(4);

                //retrieving the distance specific data from each leg
                JSONObject jsonDistance0 = jsonLeg0.getJSONObject("distance");
                JSONObject jsonDistance1 = jsonLeg1.getJSONObject("distance");
                JSONObject jsonDistance2 = jsonLeg2.getJSONObject("distance");
                JSONObject jsonDistance3 = jsonLeg3.getJSONObject("distance");
                JSONObject jsonDistance4 = jsonLeg4.getJSONObject("distance");

                //retrieving the duration specific data from each leg
                JSONObject jsonDuration0 = jsonLeg0.getJSONObject("duration");
                JSONObject jsonDuration1 = jsonLeg1.getJSONObject("duration");
                JSONObject jsonDuration2 = jsonLeg2.getJSONObject("duration");
                JSONObject jsonDuration3 = jsonLeg3.getJSONObject("duration");
                JSONObject jsonDuration4 = jsonLeg4.getJSONObject("duration");

                /*
                //retrieving the directions specific data from each leg
                String jsonDirection0 = jsonLeg0.getString("instructions");
                String jsonDirection1 = jsonLeg1.getString("instructions");
                String jsonDirection2 = jsonLeg2.getString("instructions");
                String jsonDirection3 = jsonLeg3.getString("instructions");
                String jsonDirection4 = jsonLeg4.getString("instructions");
*/
                JSONObject jsonEndLocation = jsonLeg4.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg0.getJSONObject("start_location");

                //Error Checking
                //Log.i("Length", "" + jsonLegs.length());
  //              Log.i("Array 0", " " + jsonDirection0);
    //            Log.i("Array 1", " " + jsonDirection1);
                //Log.i("Array 2", " " + jsonLeg2);
                //Log.i("Array 3", " " + jsonLeg3);
                //Log.i("Array 4", " " + jsonLeg4);

                //getting the total distance (in metres)
                int TotalDistance = jsonDistance0.getInt("value") +
                        jsonDistance1.getInt("value") +
                        jsonDistance2.getInt("value") +
                        jsonDistance3.getInt("value") +
                        jsonDistance4.getInt("value");

                //getting the total duration (in seconds)
                int TotalDuration = jsonDuration0.getInt("value") +
                        jsonDuration1.getInt("value") +
                        jsonDuration2.getInt("value") +
                        jsonDuration3.getInt("value") +
                        jsonDuration4.getInt("value");

      //          String TotalDirections = jsonDirection0 + jsonDirection1 + jsonDirection2 +jsonDirection3 +jsonDirection4;

                //Convert to KM
                TotalDistance = TotalDistance / 1000;

                //Convert to hrs and mins
                int Hours = TotalDuration / 3600;
                int Minutes = (TotalDuration / 60) - (Hours * 60);


                //Error Checking
                //Log.i("Total Duration: " + TotalDuration, "Total Distance: " + TotalDistance);

                //Send to Route, Distance and Duration Methods
                route.distance = new Distance(TotalDistance + " KM", TotalDistance);
                route.duration = new Duration(Hours + " Hrs " + Minutes + " Mins", TotalDuration);
                route.endAddress = jsonLeg4.getString("end_address");
                route.startAddress = jsonLeg0.getString("start_address");
                route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                route.points = decodePolyLine(overview_polylineJson.getString("points"));
//                route.directions = new Directions(TotalDirections);

                          /*  jsonLeg0.getString("html_instructions") +
                            jsonLeg1.getString("html_instructions") +
                            jsonLeg2.getString("html_instructions") +
                            jsonLeg3.getString("html_instructions") +
                            jsonLeg4.getString("html_instructions");*/

                routes.add(route);
            }
        }


        listener.onDirectionFinderSuccess(routes);
    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }
}
