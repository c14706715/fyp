//My location, San Francisco
        double lat = 37.77657;
        double lng = -122.417506;
        LatLng latLng = new LatLng(lat, lng);

        //set up list
        ArrayList<Place> places = new ArrayList<Place>();

        places.add(new Place("New York", new LatLng(40.571256,73.98369)));
        places.add(new Place("Colorado", new LatLng(39.260658,-105.101615)));
        places.add(new Place("Los Angeles", new LatLng(33.986816,118.473819)));

        for (Place p: places){
            Log.i("Places before sorting", "Place: " + p.name);
        }

        //sort the list, give the Comparator the current location
        Collections.sort(places, new SortPlaces(latLng));

        for (Place p: places){
            Log.i("Places after sorting", "Place: " + p.name);
        }