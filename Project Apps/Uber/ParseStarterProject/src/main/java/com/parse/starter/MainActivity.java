
package com.parse.starter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class MainActivity extends AppCompatActivity {

  public void redirectActivity(){
    if(ParseUser.getCurrentUser().get("ParentOrDriver").equals("Parent")){
        Intent intent = new Intent(getApplicationContext(), ParentActivity.class);
        startActivity(intent);
    }
    else{
        Intent intent = new Intent(getApplicationContext(), ViewRequestsActivity.class);
        startActivity(intent);
    }
  }

  public void getStarted(View view){
    Switch userTypeSwitch = (Switch) findViewById(R.id.userTypeSwitch);
    Log.i("Switch value", String.valueOf(userTypeSwitch.isChecked()));

    String userType = "Parent";

    //Checks if the switch is set to true or false i.e. parent or Driver
    if(userTypeSwitch.isChecked()){
      userType = "Driver";
    }

    //Checks if user is parent or driver, then .saveInBackground will auto save this setting for the
    //next time the user logs in
    ParseUser.getCurrentUser().put("ParentOrDriver", userType);
    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        redirectActivity();
      }
    });

    Log.i("Info", "Redirecting as " +userType);

    redirectActivity();
  }



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getSupportActionBar().hide();

    //Using the parse server to check if the user has logged in;
    //simple success or fail message display to the console
    if(ParseUser.getCurrentUser() == null){
      ParseAnonymousUtils.logIn(new LogInCallback() {
        @Override
        public void done(ParseUser user, ParseException e) {
          if(e == null){
            Log.i("Info", "Anonymous login successful");
          }
          else{
            Log.i("Info", "Anonymous login failed");
          }
        }
      });
    }
    //Checks if the user is a parent or driver once rather than everytime checking
    else{
      if(ParseUser.getCurrentUser().get("ParentOrDriver") != null){
          Log.i("Info", "Redirecting as " + ParseUser.getCurrentUser().get("ParentOrDriver"));
          redirectActivity();
      }
    }
  }
}